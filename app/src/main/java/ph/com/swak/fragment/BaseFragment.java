package ph.com.swak.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;

/**
 * Created by SWAK-THREE on 4/6/2015.
 */
public class BaseFragment extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
}
