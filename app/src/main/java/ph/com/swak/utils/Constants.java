package ph.com.swak.utils;

/**
 * Created by SWAK-THREE on 3/25/2015.
 */
public interface Constants {
    // Past
    // public String URL = "http://192.168.2.64:3231";

    // Live
    // public String URL = "http://bbcpayroll.swak.net.ph";

    // Emulator, Bluestack, Android Studio
    public String URL = "http://10.0.2.2:3231";

    // Mobile URL
    // public String URL = "http://localhost:3231";

    // TEMP MAX CONSTANTS
    public int MAX_DAYS = 100;
    public int MAX_HOURS = 10000;

    public static class Extra {
        public static final String FRAGMENT_INDEX = "ph.com.swak.FRAGMENT_INDEX";
        public static final String IMAGE_POSITION = "ph.com.swak.IMAGE_POSITION";
    }

}
